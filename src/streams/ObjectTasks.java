package streams;

import java.util.Arrays;
import java.util.stream.Collectors;

public class ObjectTasks {
    public static String task1(Purchase[] purchases) {
        return Arrays.stream(purchases)
                .collect(Collectors.groupingBy(x -> x.buyer,
                        Collectors.reducing(0,
                                x -> x.count,
                                (x, y) -> x + y)))
                .entrySet().stream()
                .max((x, y) -> Integer.compare(x.getValue(), y.getValue()))
                .map(x -> x.getKey())
                .orElse("None");
    }

    public static String[] task2(Purchase[] purchases, Product[] products) {
        var map = Arrays.stream(products)
                .collect(Collectors.toMap(
                        x -> x.name + ":" + x.shop,
                        x -> x.price
                ));

        return Arrays.stream(purchases)
                .collect(Collectors.groupingBy(
                        x -> x.buyer,
                        Collectors.mapping(
                                x -> x.count * map.get(x.product + ":" + x.shop),
                                Collectors.summingInt(p -> p))
                ))
                .entrySet().stream()
                .map(x -> x.getKey() + " - " + x.getValue())
                .toArray(String[]::new);
    }

    public static String[] task3(Purchase[] purchases, Product[] products) {
        var map = Arrays.stream(products)
                .collect(Collectors.toMap(
                        x -> x.name + ":" + x.shop,
                        x -> x.price
                ));

        return Arrays.stream(purchases)
                .collect(Collectors.groupingBy(
                        x -> x.product,
                        Collectors.reducing(
                                new int[] {0, 0},
                                x -> new int[] {x.count, map.get(x.product + ":" + x.shop)},
                                (x, y) -> new int[] {x[0] + y[0], Math.max(x[1], y[1])}
                        )
                ))
                .entrySet().stream()
                .map(x -> x.getKey() + " - " + x.getValue()[0] + " - " + x.getValue()[1])
                .toArray(String[]::new);
    }

    public static String[] task4(Purchase[] purchases, Product[] products) {
        var map = Arrays.stream(products)
                .collect(Collectors.toMap(
                        x -> x.name + ":" + x.shop,
                        x -> x.price
                ));

        return Arrays.stream(purchases)
                .collect(Collectors.groupingBy(
                        x -> x.shop + " - " + x.buyer,
                        Collectors.summingInt(x -> x.count * map.get(x.product + ":" + x.shop))
                ))
                .entrySet().stream()
                .map(x -> x.getKey() + " - " + x.getValue())
                .toArray(String[]::new);
    }
}