package streams;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class SimpleTasks {
    public static void task1(String input) {
        Arrays.stream(input.toLowerCase().split(" "))
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()))
                .forEach((key, value) -> System.out.println(key + " - " + value));
    }

    public static void task2(String input) {
        Arrays.stream(input.toLowerCase().split(" "))
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()))
                .entrySet().stream()
                .sorted((x, y) -> Long.compare(x.getValue(), y.getValue()))
                .forEach(x -> System.out.println(x.getKey() + " - " + x.getValue()));
    }

    public static List<Integer> task3(Integer[] a, Integer[] b, int k1, int k2) {
        return Stream.concat(
                Arrays.stream(a).filter(x -> x < k1),
                Arrays.stream(b).filter(x -> x > k2))
                .sorted((x, y) -> -Integer.compare(x, y))
                .collect(Collectors.toList());
    }

    public static List<String> task4(String[] a, String[] b, int k1, int k2) {
        return Stream.concat(
                Arrays.stream(a).filter(s -> s.length() > k1),
                Arrays.stream(b).filter(s -> s.length() < k2))
                .sorted((x, y) -> -x.compareTo(y))
                .collect(Collectors.toList());
    }

    public static List<Integer> task5(Integer[] a) {
        return Arrays.stream(a)
                .collect(Collectors.groupingBy(x -> x % 10,
                        Collectors.maxBy(Integer::compareTo)))
                .values().stream()
                .filter(x -> x.isPresent())
                .map(x -> x.get())
                .sorted((x, y) -> Integer.compare(x % 10, y % 10))
                .collect(Collectors.toList());
    }

    public static Map<Integer, Integer> task6(Integer[] a) {
        return Arrays.stream(a)
                .collect(Collectors.groupingBy(x -> x % 10,
                        Collectors.summingInt(x -> x)));
    }
}