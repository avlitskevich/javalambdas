package streams;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


class Product {
    public String name;
    public String shop;
    public int price;

    public Product(String name, String shop, int price) {
        this.name = name;
        this.shop = shop;
        this.price = price;
    }
}

class Purchase {
    public String buyer;
    public String product;
    public String shop;
    public int count;

    public Purchase(String buyer, String product, String shop, int count) {
        this.buyer = buyer;
        this.product = product;
        this.shop = shop;
        this.count = count;
    }
}

public class Main {
    public static void main(String[] args) {
    }

    public static void task1(String input) {
        Arrays.stream(input.split(" "))
                .collect(Collectors.groupingBy(x -> x, Collectors.counting()))
                .forEach((key, value) -> System.out.println(key + " - " + value));
    }

    public static void task2(String input, int n) {
        Arrays.stream(input.split(" "))
                .collect(Collectors.groupingBy(x -> x, Collectors.counting()))
                .entrySet().stream()
                .sorted((x, y) -> -Long.compare(x.getValue(), y.getValue()))
                .limit(n)
                .forEach(x -> System.out.println(x.getKey()));
    }

    public static List<Integer> task3(Integer[] a, Integer[] b, int k1, int k2) {
        return Stream.concat(
                    Arrays.stream(a).filter(x -> x < k1),
                    Arrays.stream(b).filter(x -> x > k2)
                ).toList();
    }

    public static List<String> task4(String[] a, String[] b, int k1, int k2) {
        return Stream.concat(
                Arrays.stream(a).filter(x -> x.length() < k1),
                Arrays.stream(b).filter(x -> x.length() > k2)
        ).toList();
    }

    public static List<Integer> task5(Integer[] a) {
        return Arrays.stream(a)
                .collect(Collectors.groupingBy(x -> x % 10))
                .values().stream()
                .map(x -> x.stream().max(Integer::compare))
                .filter(x -> x.isPresent())
                .map(x -> x.get())
                .toList();
    }

    public static Map<Integer, Integer> task6(Integer[] a) {
        return Arrays.stream(a)
                .collect(Collectors.groupingBy(x -> x % 10))
                .entrySet().stream()
                .collect(Collectors.toMap(x -> x.getKey(),
                        x -> x.getValue().stream().reduce(0, Integer::sum)));
    }
}