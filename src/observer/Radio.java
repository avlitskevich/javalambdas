package observer;

public class Radio implements Electricity {
    @Override
    public void electricityOn() {
        System.out.println("Music is playing");
    }
}