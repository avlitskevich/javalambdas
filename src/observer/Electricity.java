package observer;

public interface Electricity {
    void electricityOn();
}