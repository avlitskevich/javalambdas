package observer;

import java.util.ArrayList;

public class Switcher {
    private ArrayList<Electricity> listeners = new ArrayList<>();

    public void switchOn() {
        System.out.println("Выключатель включён");

        for (Electricity listener : listeners)
            listener.electricityOn();
    }

    public void addListener(Electricity listener) {
        if (!listeners.contains(listener))
            listeners.add(listener);
    }

    public void removeListener(Electricity listener) {
        if (listeners.contains(listener))
            listeners.remove(listener);
    }
}