package observer;

public class Main {
    public static void main(String[] args) {
        Switcher switcher = new Switcher();

        Lamp lamp1 = new Lamp();
        Lamp lamp2 = new Lamp();
        Radio radio = new Radio();

        switcher.addListener(lamp1);
        switcher.addListener(lamp2);
        switcher.addListener(radio);
        switcher.addListener(() -> System.out.println("Пожар!"));

        switcher.switchOn();
    }
}