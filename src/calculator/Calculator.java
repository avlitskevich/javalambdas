package calculator;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Calculator {
    private Map<Character, Operation> operations;

    public Calculator() {
        operations = new HashMap<>();

        // fill operations here
    }

    public void processInput(String input) {
        String[] splitted = input.split(" ");

        Stack<Integer> stack = new Stack<>();

        for (String s : splitted) {
            if (isNumber(s)) {
                stack.push(Integer.parseInt(s));
                continue;
            }

            Character c = s.charAt(0);
            if (!operations.containsKey(c))
                throw new UnsupportedOperationException();

            int a = stack.pop();
            int b = stack.pop();
            int result = operations.get(c).calculate(a, b);

            stack.push(result);
        }
    }

    private boolean isNumber(String x) {
        try {
            Integer.parseInt(x);
            return true;
        }
        catch (NumberFormatException e) {
            return false;
        }
    }
}