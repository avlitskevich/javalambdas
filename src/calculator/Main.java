package calculator;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.function.Function;
import java.util.function.Predicate;

public class Main {
    static int counter = 10;

    public static void main(String[] args) {
//        Calculator calculator = new Calculator();
//        Scanner in = new Scanner(System.in);
//
//        calculator.processInput(in.nextLine());
//
//        ArrayList<Integer> list = new ArrayList<>();
//
//        ArrayList<Integer> evenNumbers = siftList(list, x -> x % 2 == 0);
//        ArrayList<Integer> greaterThanZero = siftList(list, x -> x > 0);
//
//        ArrayList<String> strings = convertToString(list, Object::toString);

//        Printable mainPrint = Main::printInt;
//
//        mainPrint.print(10);
//        mainPrint.print(20);
//        mainPrint.print(30);
//        mainPrint.print(140);


    }

    public static void printInt(int x) {
        counter++;
        System.out.println("I'm writing int value: " + x + " for " + counter + " time");
    }

    public static ArrayList<Integer> siftList(ArrayList<Integer> list, Predicate<Integer> predicate) {
        ArrayList<Integer> result = new ArrayList<>();

        for (Integer x : list) {
            if (predicate.test(x))
                result.add(x);
        }

        return result;
    }

    public static <T, R> ArrayList<R> convertToString(ArrayList<T> list, Function<T, R> func) {
        ArrayList<R> result = new ArrayList<>();

        for (T x : list)
            result.add(func.apply(x));

        return result;
    }
}