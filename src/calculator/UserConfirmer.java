package calculator;

import java.util.Scanner;

public class UserConfirmer {
    private Scanner in = new Scanner(System.in);

    public void askToConfimAction(Action callback) {
        System.out.println("Are you sure? Enter y for yes, n for no");
        String answer = in.nextLine();

        if (answer.equals("y"))
            callback.invoke();
    }
}