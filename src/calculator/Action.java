package calculator;

public interface Action {
    void invoke();
}