package calculator;

public interface Calculation {
    void processInput(String input);
}