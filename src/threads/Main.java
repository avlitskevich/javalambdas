package threads;

public class Main extends Thread {
    public static void main(String[] args) {
//        MyThread myThread = new MyThread();
//        myThread.start();

//        Thread thread = new Thread(new MyRunnable());
//        thread.start();

//        Thread thread = new Thread(() -> System.out.println("This is a new thread from lambda!!"));
//        thread.start();

//        Thread thread = new Thread(Main::runNewThread);
//        thread.start();

//        for (int i = 0; i < 10; i++) {
//            MyThread myThread = new MyThread("Thread " + i);
//            myThread.start();
//        }

//        MyThread myThread = new MyThread("My thread");
//        myThread.start();
//        try {
//            myThread.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

//        MyStoppingThread myStoppingThread = new MyStoppingThread();
//        myStoppingThread.start();
//
//        try {
//            Thread.sleep(100);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        myStoppingThread.stopThread();

        MyThread myThread = new MyThread("MyThread");
        System.out.println(myThread.getState());
        myThread.start();
        System.out.println(myThread.getState());
        //some actions
        myThread.interrupt();
        System.out.println(myThread.getState());


        System.out.println("Main thread is finished");
    }

    public static void runNewThread() {
        System.out.println("New thread from method :: reference!!");
    }
}