package threads;

public class MyStoppingThread extends Thread {
    private boolean isActive = true;

    @Override
    public void run() {
        while (isActive) {
            System.out.println("Thread is executing...");

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void stopThread() {
        isActive = false;
    }
}