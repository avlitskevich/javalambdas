package threads;

public class MyInterruptingThread extends Thread {
    @Override
    public void run() {
        while (!isInterrupted()) {
            System.out.println("Thread is executing...");

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
                interrupt();
            }
        }
    }
}