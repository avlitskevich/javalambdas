package builder;

public class Main {
    public static void main(String[] args) {
        User user = User.builder()
                .withFirstName("Bob")
                .withId(10)
                .withEmail("example@email.com")
                .build();
    }
}