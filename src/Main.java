import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    static Random r = new Random();

    static String[] products = new String[] {"книга", "ноутбук", "ручка", "шкаф", "стол", "стул"};
    static String[] buyers = new String[] {"Боб", "Мартин", "Игорь", "Вася", "Петя", "Джон", "Джейкоб", "Олег"};

    static void generateTask3(int n) {
        try (FileOutputStream fout = new FileOutputStream("task3.txt");
             PrintWriter writer = new PrintWriter(fout)){
            for (int i = 0; i < n; i++) {
                String product = products[r.nextInt(products.length)];
                int count = r.nextInt(9) + 1;
                writer.write(product + "|" + count + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void generateTask4(int n) {
        try (FileOutputStream fout = new FileOutputStream("task4.txt");
             PrintWriter writer = new PrintWriter(fout)){
            for (int i = 0; i < n; i++) {
                String buyer = buyers[r.nextInt(buyers.length)];
                String product = products[r.nextInt(products.length)];
                int count = r.nextInt(9) + 1;
                writer.write(buyer + "|" + product + "|" + count + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Map<String, Integer> task3() {
        Map<String, Integer> result = new HashMap<>();

        try {
            Stream<String> stream = Files.lines(Paths.get("task3.txt"));

            stream.map(x -> x.split("\\|"))
                    .collect(Collectors.groupingBy(x -> x[0]))
                    .forEach((key, value) -> result.put(
                            key,
                            value.stream()
                                    .reduce(0,
                                            (a, b) -> a + Integer.parseInt(b[1]),
                                            Integer::sum)));
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static void main(String[] args) {
        task3().forEach((x, y) -> System.out.println(x + " - " + y));
    }
}